This is an issue repository for bugs, feature request, etc. of non disclosed OnSive.net projects.

| Project Name | Version | NuGet |
| ------------ |:-------:|:-----:|
| OnSive.Core.Console | [![NuGet stable version](https://badgen.net/nuget/v/OnSive.Core.Console)](https://nuget.org/packages/OnSive.Core.Console) | [![NuGet stable version](https://badgen.net/nuget/dt/OnSive.Core.Console)](https://nuget.org/packages/OnSive.Core.Console) |
| OnSive.Core.Discord | [![NuGet stable version](https://badgen.net/nuget/v/OnSive.Core.Discord)](https://nuget.org/packages/OnSive.Core.Discord) | [![NuGet stable version](https://badgen.net/nuget/dt/OnSive.Core.Discord)](https://nuget.org/packages/OnSive.Core.Discord) |
| OnSive.Core.EntityFrameworkCore | [![NuGet stable version](https://badgen.net/nuget/v/OnSive.Core.EntityFrameworkCore)](https://nuget.org/packages/OnSive.Core.EntityFrameworkCore) | [![NuGet stable version](https://badgen.net/nuget/dt/OnSive.Core.EntityFrameworkCore)](https://nuget.org/packages/OnSive.Core.EntityFrameworkCore) |
| OnSive.Core.Helper | [![NuGet stable version](https://badgen.net/nuget/v/OnSive.Core.Helper)](https://nuget.org/packages/OnSive.Core.Helper) | [![NuGet stable version](https://badgen.net/nuget/dt/OnSive.Core.Helper)](https://nuget.org/packages/OnSive.Core.Helper) |

![ForTheBadge powered-by-electricity](http://ForTheBadge.com/images/badges/powered-by-electricity.svg)
